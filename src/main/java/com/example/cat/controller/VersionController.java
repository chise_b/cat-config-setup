package com.example.cat.controller;

import com.configcat.ConfigCatClient;
import com.example.cat.domain.Version;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class VersionController {

    private static final ConfigCatClient client = new ConfigCatClient("M57ZCGaaVEGEEmQQsQOXOA/fT_MnumVTEWi-zQzDZwhvA");

    @GetMapping("/version")
    public ResponseEntity<Version> getVersion() {
        Version oldVersion = new Version("v1");
        boolean newApi = client.getValue(Boolean.class, "newApi", false);
        if (newApi){
            Version newVersion = new Version("v2");
            return ResponseEntity.ok(newVersion);
        }
        return ResponseEntity.ok(oldVersion);
    }

}
