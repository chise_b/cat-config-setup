package com.example.cat.domain;

import java.io.Serializable;

public class Version implements Serializable {

    private String apiVersion;

    public Version(String apiVersion){
        this.apiVersion = apiVersion;
    }

    public String getApiVersion() {
        return apiVersion;
    }

    public void setApiVersion(String apiVersion) {
        this.apiVersion = apiVersion;
    }
}
