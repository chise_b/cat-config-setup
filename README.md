# ConfigCat Basic Setup

This is a simple ConfigCat Spring Boot sample app 
which enables different version based on a feature flag.  
Note that the feature flag is enabled only in **Test** environment!

